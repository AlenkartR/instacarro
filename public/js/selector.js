/* 
	Alenkart Rodriguez
	1/12/2018

	Warpping the querySelector and using the dollar sign "$"" to simulate 
	the popular jQuery syntax.
*/

'use strict';

(function(window, undefined) {

	function selector(elementSelector) {

		var element = document.querySelector(elementSelector);

		//Exporting the publics methods
		return {

			//get the dom element
			get : function() {

				return element;
			}, 

			hasClass :  function(className) {

				if(typeof className === 'string') {

					return element.className.indexOf(className) !== -1;
				}
				
				return false;
			},

			//change the innerHtml
			html : function(string) {
				
				if(string) {

					element.innerHTML = string;

					return this;
				}
			
				return element.innerHTML;
			},

			//add events
			on : function(event, callback) {

				element.addEventListener(event, callback);

				return this;
			},

			// on click event
			click : function(callback) {

				if(!callback) {
				
					element.click();
				} else {
				
					this.on('click', callback);
				}
				
				return this;
			},

			//remove content
			empty : function() {
				
				if( element.innerHTML ) {

					element.innerHTML = "";
				}

				return this;
			},

			//add element at the end
			append : function(htmlString) {

				element.innerHTML += htmlString;

				return this;
			},
			
			//find a child element and return a selector
			find : function(childSelector) {

				return selector(elementSelector+' '+childSelector);
			},

			//set a css style
			css : function (style, value) {
	
				var type = typeof style;
	
				if(type === 'object') {

					Object.keys(style).forEach(function(key) { 
						element.style[key] = style[key];
					});
	
				} else if (type === 'string' && value === 'string') {
	
					element.style[style]  = value;

				} else if(type === 'string') {

					return window.getComputedStyle(element, null)
						.getPropertyValue(style);
				} 
				
				return this;
			},

			//Do a http request
			fecth : function(config) {
	
				//Setting default values
				config.method = config.method || 'GET';
	
				config.type = config.type || "text/plain; charset=x-user-defined";
	
				if( !!config.json ) {
					config.type = "application/json";
				} 
	
				//Instance the xmlhttprequest
				var xhttp = new XMLHttpRequest();
	
				//Passing the configuration
				xhttp.overrideMimeType(config.type);
				xhttp.open(config.method, config.uri, true);
	
				//Beding the callback methods
				xhttp.onerror = config.error;
	
				xhttp.onload = function(res) {
	
					if(xhttp.readyState === 4 && xhttp.status === 200)  {
	
						var response = xhttp.responseText;	
						
						response = !!config.json 
							? JSON.parse(response) 
							: response;
	
						config.success(response);
					}
					
				};
	
				xhttp.send(null);
			}
		}
	}

	//Exporting the Selector || "$" object. 
	window.selector = window.selector || selector;
	window.$ = window.$ || selector;

})(window);