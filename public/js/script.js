'use strict';

(function(windows, $, undefined) {

	const document = window.document;

	/*
		Wating to the windows load before execute the script.
	*/
	function onload() {

		const config = { 
			json : true,
			uri : 'https://www.alenkart.com/instacarro/data',
			success : function(res) {
				carousel('#carousel-1', 'Carros populares vendidos perto de voce', res);
				carousel('#carousel-2', 'Modelos de carros mais vendidos', res);
			},
		};

		selector().fecth(config);
	}

	window.onload = onload;
	
})(window, selector);