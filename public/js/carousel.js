(function(window, $, undefined) {

    function carousel(selector, title, slidesData) {

        var $carousel = $(selector);

        var $header = $carousel.find('.carousel__header');            
        var $body = $carousel.find('.carousel__body');
        
        //Setting the title
        $header.html(title);

        //Adding the traking
        $body.append('<div class="carousel__tracker"></div>');

        addButtons();
        
        var $tracker = $body.find('.carousel__tracker');
        var $buttonLeft = $carousel.find('.carousel__button--left');
        var $buttonRight = $carousel.find('.carousel__button--right');

        addSlides();

        var $slide = $tracker.find('.carousel__slide');

        //Adding the buttons to the $carousel
        function addButtons() {
            $carousel.append([
                '<div class="carousel__button carousel__button--left"></div>',
                '<div class="carousel__button carousel__button--right"></div>'
            ].join(''));
        }
    
        //Adding the slides to the tracker
        function addSlides() {

            slidesData.forEach(function(slideData) {
                $tracker.append( getSlide(slideData, 15 * slideData.stars) );
            });

        }

        function toUpperCase(word) {

            if(!word || typeof word !== 'string') {
                return "";
            }

            return word.split(" ").map(function(w){
                return w.charAt(0).toUpperCase() + w.slice(1);
            }).join(' ');
        }

        function getDetail(slideData) {

            slideData.make = toUpperCase(slideData.make);
            slideData.model = toUpperCase(slideData.model);
            slideData.loja = toUpperCase(slideData.loja);
            slideData.state = toUpperCase(slideData.state);

            return slideData.make + ' ' + slideData.model 
            + ' ' + slideData.year + ', ' + slideData.loja + ' ' +slideData.state;
        }

        function getSlide(slideData, width) {

            var formatter = new Intl.NumberFormat('pt-br', {
                style: 'currency',
                currency: 'BRL',
                minimumFractionDigits: 0,
            });

            var detail = getDetail(slideData);

            var price = formatter.format(slideData.price || 0);

            var style = 'style="width:'+ width+'px"';

            return [
                '<div class="carousel__slide">',
                    '<div class="slide__img">',
                        '<img src="'+slideData.uri+'">',
                    '</div>',
                    '<div class="slide__detail">',
                    '<div>',
                        '<span class="slide__price">'+price+'</span>',
                        '<span class="slide__text">'+detail+'</span>',
                    '</div>',
                    '<div class="slide__rating">',
                        '<div class="rating__top" '+style+'>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                        '</div>',
                        '<div class="rating__bottom">',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                            '<span class="rating__start"></span>',
                        '</div>',
                        '<span class="rating__text">'+slideData.bids+' lances</span>',
                    '</div>',
                    '</div>',
                '</div>'
            ].join('');
        }

        function createInterval() {
            return setInterval($buttonRight.click, 5000);
        }

        function translate(counter, width) {

            var translateX = width * counter;

            $tracker.css({ 
                transform: 'translatex('+ translateX +'px)' 
            });
        }

        function getMAx() {
            return Math.round($carousel.get().offsetWidth / $slide.get().offsetWidth);
        }

        function getWidth() {
            var slideWidth = $slide.get().offsetWidth;
            var slideMarginRigth = + $slide.css('margin-right').replace(/[^0-9]+/g, "");
            return slideWidth + slideMarginRigth;
        }

        function start() {

            var counter = 0;
            var max = getMAx();
            var width = getWidth();
            var childs = $tracker.get().childNodes.length;
            var interval = createInterval();
            
            //Setting the onClickEvent
            $buttonLeft.click(function() {
                
                counter = counter + max > -1 ? 0 : counter+1;
                translate(counter, width);
            });

            $buttonRight.click(function() {

                counter = ((counter - max) > - childs) 
                    ? counter -1
                    : 0;

                translate(counter, width);    
            });

            window.addEventListener('resize', function() {
                counter = 0;
                max = getMAx();
                width = getWidth();
                translate(counter, width);
            });
        }

        start();

        return {
            start : start,
        }

    }

    window.carousel = window.carousel || carousel;

})(window, selector || $);