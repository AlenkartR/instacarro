'use strict';

require('./../public/js/selector');
require('./../public/js/carousel');
const apiRes = require('./../public/js/api-response');

describe('Test the carousel api', () => {

    document.body.innerHTML =
    '<div class="container">' +
        '<div id="carousel-1" class="carousel">' +
            '<div class="carousel__header"></div>' +
            '<div class="carousel__body"></div>' +
        '</div>'+
    '</div>';    

    it('Adds the carousel to the dom ', () => {
        
        carousel('#carousel-1', 'Alenkart', apiRes);

        const slides = document.querySelectorAll('.carousel__slide');

        expect(slides.length).toBeGreaterThan(0);

    }); 
});