'use strict';

require('./../public/js/selector');

const resetTheDom = () => {
    
    document.body.innerHTML =
    '<div class="profile">' +
        '<span id="username">Alenkart Rodriguez</span>' +
        '<span id="age">22</span>' +
    '</div>';    
}

describe('Test the selector api', () => {

    it('Test the html method', () => {

        resetTheDom();

        const username = $('#username').html();

        expect(username).toEqual('Alenkart Rodriguez');
    });


    it('Test the hasClass method', () => {

        resetTheDom();

        const hasClass = $('.profile').hasClass('profile');
        
        expect(hasClass).toBeTruthy();
    });

    it('Test the empty method', () => {

        resetTheDom();

        const age = $('#age').empty().html();

        expect(age).toEqual("");
    });

    it('Test the find method', () => {

        resetTheDom();
        
        const username = $('.profile').find('#username').html();

        expect(username).toEqual('Alenkart Rodriguez');
    });

    it('Test the append method', () => {

        resetTheDom();
        
        const $profile = $('.profile');

        $profile.append('<span id="country">Dominican Republic</span>');

        const $country = $profile.find('#country');

        expect($country.html()).toEqual('Dominican Republic');
    });

    it('Test the css method', () => {

        resetTheDom();

        const $profile = $('.profile');

        $profile.css({
            width : '20px'
        });

        const width = $profile.css('width');

        expect(width).toEqual('20px');
    });

    it('Test the fecth method', () => {
        
        resetTheDom();

        function selectorFecth() {

            return new Promise((res, rej) => {
    
                const $profile = $().fecth({
                    json : true,
                    uri : 'https://www.alenkart.com/instacarro/data',
                    success : function(response) {
                        res(response);
                    },
                    error: function (err) {
                        rej(err);
                    },
                });
    
            });
        }

        return selectorFecth().then(res => expect(res).toBeInstanceOf(Array) );
        
    });

})
