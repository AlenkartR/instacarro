## Online Demo
[https://www.alenkart.com/instacarro/](https://www.alenkart.com/instacarro/)

## Selector "$"
A mini JavaScript library created for this project its mainly use as a warper to multiple common functionalities like DOM manipulation for example. I want to clarify that this ins't "jQuery", the lib just use the dollar "$" sign tu mimic it's popular syntax. 

## Test Cases
Before run the test cases please install dev dependencies with the package manager of your preference. The tests are are written in Jest, use the following commands to run the tests:

yarn
```
 yarn
 yarn test
```
